/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : bbs

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-19 16:09:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for topic
-- ----------------------------
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` int(11) NOT NULL DEFAULT '0' COMMENT '话题发表人id',
  `moduleID` int(11) NOT NULL COMMENT '所属分类Id',
  `postCount` int(11) NOT NULL DEFAULT '1' COMMENT '回复数量（楼层数量）',
  `replyCount` int(11) NOT NULL DEFAULT '0' COMMENT '回复数量（与postCount重复）',
  `pv` int(11) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `content` varchar(60) NOT NULL COMMENT '标题',
  `emotion` tinyint(2) DEFAULT NULL,
  `createTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isNice` bit(1) NOT NULL DEFAULT b'0' COMMENT '精华帖子',
  `isUp` bit(1) NOT NULL DEFAULT b'0' COMMENT '置顶',
  PRIMARY KEY (`id`),
  KEY `moduleID_T` (`moduleID`),
  KEY `userID_T` (`userID`),
  CONSTRAINT `moduleID_T` FOREIGN KEY (`moduleID`) REFERENCES `module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userID_T` FOREIGN KEY (`userID`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : bbs

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-19 16:08:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT NULL COMMENT '分类名称',
  `detail` varchar(100) DEFAULT NULL COMMENT '分类描述',
  `turn` tinyint(2) DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', '=====我是分类1=====', null, '1');
INSERT INTO `module` VALUES ('2', '=====我是分类2=====', null, '2');
INSERT INTO `module` VALUES ('3', '=====我是分类3=====', null, '3');
INSERT INTO `module` VALUES ('4', '=====我是分类4=====', null, '4');
INSERT INTO `module` VALUES ('5', '=====我是分类5=====', null, '5');
INSERT INTO `module` VALUES ('6', '=====我是分类6=====', null, '6');
INSERT INTO `module` VALUES ('7', '=====我是分类7=====', null, '7');

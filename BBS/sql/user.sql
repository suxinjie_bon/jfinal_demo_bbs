/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : bbs

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-19 16:10:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(40) NOT NULL COMMENT '密码',
  `sex` bit(1) NOT NULL DEFAULT b'1' COMMENT '性别',
  `email` varchar(30) DEFAULT NULL COMMENT '电子邮件',
  `headImg` varchar(120) DEFAULT 'http://www.qqhot.com/d/file/touxiang/nvsheng/2015-05-15/5806349844be3fc0f7fbe9630c72d35e.jpg' COMMENT '头像地址',
  `blogUrl` varchar(80) DEFAULT NULL COMMENT '博客地址',
  `feeling` varchar(300) DEFAULT NULL COMMENT '个性签名',
  `registDate` date DEFAULT NULL COMMENT '注册日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

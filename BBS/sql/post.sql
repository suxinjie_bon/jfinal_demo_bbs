/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : bbs

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-05-19 16:09:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `topicID` int(11) NOT NULL COMMENT '话题id',
  `userID` int(11) NOT NULL DEFAULT '0' COMMENT '回复人id',
  `content` text NOT NULL COMMENT '回复内容',
  `createTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `hasReply` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否回复',
  `updateTime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topicID_P` (`topicID`),
  KEY `userID_P` (`userID`),
  CONSTRAINT `topicID_P` FOREIGN KEY (`topicID`) REFERENCES `topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userID_P` FOREIGN KEY (`userID`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

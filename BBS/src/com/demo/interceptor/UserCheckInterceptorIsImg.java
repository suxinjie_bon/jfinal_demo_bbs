package com.demo.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;

public class UserCheckInterceptorIsImg implements Interceptor {

	@Override
	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		UploadFile file = controller.getFile();
		controller.setAttr("imgFile", file);
        String userID = controller.getSessionAttr("userID").toString();
        String paraUserId = controller.getPara(0)==null ? controller.getPara("user.id","0") : controller.getPara(0, "0");
        if(StrKit.notBlank(userID) && userID.equals(paraUserId)){
            ai.invoke();
        }else{
            controller.setAttr("msg", "只有该登录用户本人才有权操作");
            controller.renderError(500);
        }
	}

}

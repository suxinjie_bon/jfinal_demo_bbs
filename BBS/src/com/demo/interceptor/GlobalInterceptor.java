package com.demo.interceptor;

import com.demo.common.Constants;
import com.demo.model.Module;
import com.demo.model.User;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

public class GlobalInterceptor implements Interceptor {
    @Override
    public void intercept(ActionInvocation ai) {
        Controller controller = ai.getController();

        controller.setAttr("moduleList", Module.dao.getList());
        // validate user info from bbs_id
        String bbsID = controller.getCookie("bbsID");
        if( controller.getSessionAttr("user")==null && StrKit.notBlank(bbsID)){
            if(StrKit.notBlank(bbsID)){
                String[] userAndEmail = bbsID.split(Constants.BBS_ID_SEPARATOR);
                User user = null;
                if(userAndEmail != null && userAndEmail.length == 2){
                    user = User.dao.getByEmailAndPassword(userAndEmail[0], userAndEmail[1]);
                }
                if(user != null){
                    controller.getSession().setMaxInactiveInterval(1800);
                    controller.setSessionAttr("user", user);
                    controller.setSessionAttr("userID", user.get("id"));
                }else{
                    ai.getController().removeCookie("bbsID");
                }
            }
        }
        ai.invoke();
        //用来进行版本控制
        //controller.setAttr("v", Constants.TIMESTAMP);
    }
}

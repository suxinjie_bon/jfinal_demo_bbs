package com.demo.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class PostValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		c.keepPara("post.content");
		c.render("/post/save");
	}

	@Override
	protected void validate(Controller c) {
		validateString("post.content", 1, 5000, "msg", "评论不能为空且长度不能超过2000");
	}

}

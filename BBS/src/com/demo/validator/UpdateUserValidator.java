package com.demo.validator;

import com.demo.model.User;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;

public class UpdateUserValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		c.keepModel(User.class);
        c.render("/user/edit.html");
	}

	@Override
	protected void validate(Controller c) {
		validateEmail("user.email", "emailMsg", "错误的邮箱地址");
        validateRegex("user.username", "[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,8}", "usernameMsg", "用户名的长度介于2-8之间，只能包含中文，数字，字母，下划线");
        validateString("user.blogUrl", false, 0, 100, "blogUrlMsg", "长度不能超过100");
        validateString("user.feeling", false, 0, 100, "feelingMsg", "长度不能超过100");
        String email = c.getPara("user.email");
        if(StrKit.notBlank(email) && User.dao.containEmail(email)){
            addError("emailMsg", "该email已经被注册过了");
        }
        String username = c.getPara("user.username");
        if(StrKit.notBlank(username) && User.dao.containUsername(username)){
            addError("usernameMsg", "该用户名已经被注册过了");
        }
	}

}

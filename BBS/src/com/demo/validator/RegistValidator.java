package com.demo.validator;

import com.demo.common.Constants;
import com.demo.model.User;
import com.jfinal.core.Controller;
import com.jfinal.ext.render.CaptchaRender;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;

public class RegistValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		c.keepModel(User.class ,"user");
        c.render("/user/register.html");
	}

	@Override
	protected void validate(Controller c) {
		validateEmail("user.email", "emailMsg", "错误的邮箱地址");
        validateRegex("user.username", "[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,8}", "usernameMsg", "用户名的长度介于2-8之间，只能包含中文，数字，字母，下划线");
        validateRegex("user.password", "[a-zA-Z0-9_]{6,12}", "passwordMsg", "密码的长度介于6-12之间，只能包含数字，字母，下划线");
        validateEqualField("user.password", "repassword", "repasswordMsg", "2次输入的密码不一致");
        validateRequired("randomCode", "msg", "请您输入验证码!");
        
        
        String inputRandomCode = c.getPara("randomCode");
        if (StrKit.notBlank(inputRandomCode))
            inputRandomCode = inputRandomCode.toUpperCase();
         
        if (CaptchaRender.validate(c , inputRandomCode, Constants.RANDOM_CODE_KEY) == false) {
            addError("codeMsg", "验证码输入错误");
        }
        String email = c.getPara("user.email");
        if(StrKit.notBlank(email) && User.dao.containEmail(email)){
            addError("emailMsg", "该email已经被注册过");
        }
        String username = c.getPara("user.username");
        if(StrKit.notBlank(username) && User.dao.containUsername(username)){
            addError("usernameMsg", "该用户名已经被注册过");
        }
	}

}

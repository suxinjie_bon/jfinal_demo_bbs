package com.demo.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class LoginValidator extends Validator{

	@Override
	protected void handleError(Controller c) {
		 c.keepPara("email");
	     c.render("/user/login.html");
	}

	@Override
	protected void validate(Controller c) {
		validateEmail("email", "msg", "邮箱地址错误");
        validateRequired("password", "msg", "密码不能为空");
	}

}

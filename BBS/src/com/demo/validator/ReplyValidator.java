package com.demo.validator;

import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class ReplyValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		 validateString("reply.content", 1, 200, "msg", "回复内容不能为空，且不超过200字");
	}

	@Override
	protected void validate(Controller c) {
		c.keepPara("reply.content");
		c.render("/topic/"+c.getParaToInt(0));
	}

}

package com.demo.extended.beetl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.ext.web.WebVariable;

import com.jfinal.render.Render;
import com.jfinal.render.RenderException;

public class MyBeetlRender extends Render {
	
	private GroupTemplate gt = null;
    private transient static final String encoding = getEncoding();
    private transient static final String contentType = "text/html; charset=" + encoding;

    public MyBeetlRender(GroupTemplate gt, String view) {
        this.gt = gt;
        this.view = view;
    }

	@Override
	public void render() {
		Writer writer = null;
        OutputStream os = null;
        try {
            response.setContentType(contentType);
            Template template = gt.getTemplate(view);
            Enumeration<String> attrs = request.getAttributeNames();
            while (attrs.hasMoreElements()) {
                String attrName = attrs.nextElement();
                template.binding(attrName, request.getAttribute(attrName));

            }
            WebVariable webVariable = new WebVariable();
            webVariable.setRequest(request);
            webVariable.setResponse(response);
            webVariable.setSession(request.getSession());
            template.binding("servlet", webVariable);
            template.binding("request", request);
            template.binding("ctxPath", request.getContextPath());
            if (gt.getConf().isDirectByteOutput()) {
                os = response.getOutputStream();
                template.renderTo(os);
            } else {
                writer = response.getWriter();
                template.renderTo(writer);
            }

        } catch (Exception e) {
            throw new RenderException(e);
        } finally {
            try {
                if (writer != null)
                    writer.close();
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

}

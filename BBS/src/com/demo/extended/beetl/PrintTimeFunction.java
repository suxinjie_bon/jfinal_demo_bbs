/**  
 * 文件名:    PrintTimeFunction.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月16日 上午10:42:11   
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月14日      Sue      1.0        1.0 Version  
 */ 
package com.demo.extended.beetl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.jfinal.kit.StrKit;

/**  
 * @ClassName: PrintTimeFunction   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月16日 上午10:42:11  
 */
public class PrintTimeFunction implements Function {
    private final static SimpleDateFormat TIME_STAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static SimpleDateFormat MY_DATE_FORMAT = new SimpleDateFormat("yyyy年MM月dd日");

    @Override
    public Object call(Object[] params, Context context) {
        if (params.length != 1){
            throw new RuntimeException("length of params must be 1 !");
        }
        if (StrKit.isBlank(params[0].toString())){
            return null;
        }
        String dateString = params[0].toString();
        return getMyDate(dateString);
    }

    private static String getMyDate(String dateString) {
        if (null == dateString || "".equals(dateString)) return "";
        String result = null;
        try {
            Date date = TIME_STAMP_FORMAT.parse(dateString);
            long currentTime = new Date().getTime() - date.getTime();
            int time = (int)(currentTime / 1000);
            if(time < 60) {
                result = "刚刚";
            } else if(time >= 60 && time < 3600) {
                result = time/60 + "分钟前";
            } else if(time >= 3600 && time < 86400) {
                result = time/3600 + "小时前";
            } else if(time >= 86400 && time < 864000) {
                result = time/86400 + "天前";
            } else{
                result = MY_DATE_FORMAT.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

}

package com.demo.extended.beetl;

import java.io.File;
import java.io.IOException;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.resource.WebAppResourceLoader;

import com.jfinal.kit.PathKit;
import com.jfinal.render.IMainRenderFactory;
import com.jfinal.render.Render;

public class MyBeetlRenderFactory implements IMainRenderFactory {

	public static String viewExtension = ".html";
    public static GroupTemplate gt = null;

    public MyBeetlRenderFactory() {
    	init(PathKit.getWebRootPath());
    }

    public Render getRender(String view) {
        return new MyBeetlRender(gt, view);
    }

    public String getViewExtension() {
        return viewExtension;
	}

    private void init(String root)
    {
        if(gt != null)
            gt.close();
        try
        {
            Configuration cfg = Configuration.defaultConfiguration();
            WebAppResourceLoader resourceLoader = new WebAppResourceLoader(root);
            gt = new GroupTemplate(resourceLoader, cfg);
        }
        catch(IOException e)
        {
            throw new RuntimeException("\u52A0\u8F7DGroupTemplate\u5931\u8D25", e);
        }
    }
}

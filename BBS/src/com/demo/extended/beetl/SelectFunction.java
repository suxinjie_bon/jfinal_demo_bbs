package com.demo.extended.beetl;

import org.beetl.core.Context;
import org.beetl.core.Function;

import com.jfinal.kit.StrKit;

public class SelectFunction implements Function {

	@Override
	public Object call(Object[] params, Context context) {
		
		if(params.length != 3)
			throw new RuntimeException("length of params must be 3..");
		
		if(StrKit.notNull(params[0] ,params[1])){
			String param1 = params[0].toString();
			String param2 = params[1].toString();
			if(StrKit.notBlank(param1 ,param2) && param1.equals(param2))
				return params[2].toString();
		}
		
		return null;
	}

}

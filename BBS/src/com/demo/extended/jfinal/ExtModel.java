/**  
 * 文件名:    ExtModel.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午08:05:11  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.extended.jfinal;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;

/**  
 * @ClassName: ExtModel   
 * @Description: 扩展Model，操作缓存
 * @author: Sue  
 * @date:2015年5月15日 上午08:05:11    
 */
public class ExtModel<M extends Model> extends Model<M> {
	private String cacheNameForModel;
	
	public ExtModel(String cacheNameForModel){
		this.cacheNameForModel = cacheNameForModel;
	}
	
	public Page<M> loadModelPage(Page<M> page){
		List<M> modelList = page.getList();
		for(int i=0 ;i<modelList.size() ;i++){
			Model model = modelList.get(i);
			M m = loadModel(model.getInt("id"));
			modelList.set(i, m);
		}
		return page;
	}

	public M loadModel(final int id) {
		final int id_ = id;
		M m = CacheKit.get(cacheNameForModel, id, new IDataLoader() {
			
			@Override
			public Object load() {
				return findById(id_);
			}
		});
		return m;
	}
}

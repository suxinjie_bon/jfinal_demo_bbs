/**  
 * 文件名:    CommonConfig.java  
 * 描述:   Jfinal公共配置类
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月14日 下午17:18:11  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月14日      Sue      1.0        1.0 Version  
 */ 
package com.demo.common;

import com.demo.extended.beetl.MyBeetlRenderFactory;
import com.demo.interceptor.GlobalInterceptor;
import com.demo.model.Module;
import com.demo.model.Post;
import com.demo.model.Reply;
import com.demo.model.Topic;
import com.demo.model.User;
import com.demo.routes.AdminRoutes;
import com.demo.routes.FrontRoutes;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;

/**  
 * @ClassName: CommonConfig   
 * @Description: Jfinal公共配置类  
 * @author: Sue  
 * @date:2015年5月14日 下午17:18:11  
 */
public class CommonConfig extends JFinalConfig {

	public void configConstant(Constants me) {
		loadPropertyFile("config.properties");
		me.setDevMode(getPropertyToBoolean("devMode", false));
//		me.setViewType(ViewType.JSP);
		me.setBaseViewPath("/WEB-INF/bbs");
		me.setFileRenderPath("/download");
		me.setUploadedFileSaveDirectory(PathKit.getWebRootPath() + "/upload");
		me.setUrlParaSeparator("-");
		
		me.setMainRenderFactory(new MyBeetlRenderFactory());
		
//		me.setErrorView(400, "/WEB-INF/bbs/error/400.html");
//		me.setError401View("/WEB-INF/bbs/error/401.html");
//		me.setError403View("/WEB-INF/bbs/error/403.html");
//		me.setError404View("/WEB-INF/bbs/error/404.html");
//		me.setError500View("/WEB-INF/bbs/error/500.html");
	}

	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("base"));
	}

	public void configInterceptor(Interceptors me) {
		me.add(new SessionInViewInterceptor());
		me.add(new GlobalInterceptor());
	}

	public void configPlugin(Plugins me) {
		DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password"));
		me.add(druidPlugin);

		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);
		
		arp.setShowSql(true);
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		arp.addMapping("module", Module.class);
		arp.addMapping("topic", Topic.class);
		arp.addMapping("post", Post.class);
		arp.addMapping("reply", Reply.class);
		arp.addMapping("user", User.class);
		
		me.add(new EhCachePlugin());
	}

	public void configRoute(Routes me) {
		me.add(new FrontRoutes()); // 端端路由   
		me.add(new AdminRoutes()); // 后端路由
	}
	
	public void afterJFinalStart() {
		System.out.println("JFinal启动后启动自动调度线程");
	}
	
	public void beforeJFinalStop() {
		System.out.println("JFinal关闭前释放调度线程");
	}

	public static void main(String[] args) {
		JFinal.start("WebRoot", 8080, "/", 5);
	}

}

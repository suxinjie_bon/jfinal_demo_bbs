/**  
 * 文件名:    Constants.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午09:58:29  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.common;

/**  
 * @ClassName: Constants   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月15日 上午09:58:29  
 */
public class Constants {
	
	public static int TOPIC_PAGE_SIZE = 5;     // 首页帖子分页大小
	public static int POST_PAGE_SIZE = 8;       // 跟帖分页大小
    public static int REPLY_PAGE_SIZE = 5;      // 帖子回复分页大小
    public static int PAGE_SIZE_FOR_ADMIN = 30; // 管理员后台（查看帖子，回帖，跟帖）的分页大小
    // others
    public static String BBS_ID_SEPARATOR = "###";
    public static String TIMESTAMP = System.currentTimeMillis() + "";
    public static String RANDOM_CODE_KEY = "1";
}

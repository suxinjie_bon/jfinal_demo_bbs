package com.demo.controller;

import com.demo.interceptor.LoginInterceptor;
import com.demo.model.Reply;
import com.demo.validator.ReplyValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class ReplyController extends Controller {
	
	public void index(){
		if(0 == getParaToInt(1, 1))
			setAttr("replyPage", Reply.dao.getLastPage(getParaToInt(0)));
		else
		    setAttr("replyPage", Reply.dao.getPage(getParaToInt(0), getParaToInt(1, 1)));

		setAttr("postID", getParaToInt(0));
		render("/reply/reply.html");
	}
	
	//保存回复
	@Before({LoginInterceptor.class, ReplyValidator.class})
    public void save(){
        Reply reply = getModel(Reply.class);
        reply.set("userID", getSessionAttr("userID"));
        int postID = reply.getInt("postID");
        reply.mySave(postID);
        forwardAction("/reply/" + postID + "-0");
    }
}

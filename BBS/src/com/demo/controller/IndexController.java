/**  
 * 文件名:    IndexController.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:38:09  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月14日      Sue      1.0        1.0 Version  
 */
package com.demo.controller;

import com.demo.model.Topic;
import com.jfinal.core.Controller;

/**
 * @ClassName: IndexController
 * @Description:
 * @author: Sue
 * @date:2015年5月15日 上午10:38:09
 */
public class IndexController extends Controller {

	public void index() {
		setAttr("topicPage", Topic.dao.getPage(getParaToInt(0, 1)));
		setAttr("actionUrl", "/");
		render("/common/index.html");
	}

	// 留言
	public void leaveMsg() {
		render("/common/leaveMsg.html");
	}

	//注册
	public void regist() {
		render("/user/register.html");
	}
	
	//登录
	public void login() {
		render("/user/login.html");
	}

}

/**  
 * 文件名:   ModuleController.java  
 * 描述:   Jfinal公共配置类
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:26:45 
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月14日      Sue      1.0        1.0 Version  
 */ 
package com.demo.controller;

import com.jfinal.core.Controller;

/**  
 * @ClassName: ModuleController   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月15日 上午10:26:45   
 */
public class ModuleController extends Controller {
	 
}

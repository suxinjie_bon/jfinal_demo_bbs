/**  
 * 文件名:   AdminController.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:22:09
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.controller;

import com.jfinal.core.Controller;

/**  
 * @ClassName: AdminController   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月15日 上午10:22:09
 */
public class AdminController extends Controller {
	
}

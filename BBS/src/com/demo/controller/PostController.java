package com.demo.controller;

import com.demo.interceptor.LoginInterceptor;
import com.demo.model.Post;
import com.demo.model.Topic;
import com.demo.validator.PostValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;

public class PostController extends Controller {
	
	//显示某个id的文章详情
	public void index(){
		int topicId = getParaToInt(0);
		Page<Post> postPage = Post.dao.getPage(topicId, getParaToInt(1, 1));
		setAttr("postPage" ,postPage);
		setAttr("topic" ,Topic.dao.get(topicId));
		render("/post/post.html");
	}
	
	//保存评论
	@Before({LoginInterceptor.class, PostValidator.class})
    public void save(){
        Post post = getModel(Post.class);
        post.set("userID", getSessionAttr("userID"));
        post.mySave();
        redirect("/post/" + post.getInt("topicID"));
    }
}

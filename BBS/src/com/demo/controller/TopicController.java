package com.demo.controller;

import com.demo.interceptor.LoginInterceptor;
import com.demo.model.Post;
import com.demo.model.Topic;
import com.demo.validator.PostValidator;
import com.demo.validator.TopicValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class TopicController extends Controller {
	
	//新增完后跳转action
	public void index(){
		forwardAction("/post/" + getParaToInt(0));
	}
	
	//最热
	public void hot(){
		setAttr("topicPage" ,Topic.dao.getHotPage(getParaToInt(0 ,1)));
		setAttr("actionUrl", "/topic/hot/");
		render("/common/index.html");
	}
	
	//精华
	public void nice(){
		setAttr("topicPage" ,Topic.dao.getNicePage(getParaToInt(0 ,1)));
		setAttr("actionUrl", "/topic/nice/");
		render("/common/index.html");
	}

	//发表新帖
	@Before(LoginInterceptor.class)
    public void add(){
        render("/topic/add.html");
    }
	
	public void module(){
        setAttr("topicPage", Topic.dao.getPageForModule(getParaToInt(0), getParaToInt(1, 1)));
        setAttr("actionUrl", "/topic/module/" + getParaToInt(0) + "-");
        render("/common/index.html");
    }
	
	//需要save-topic和save-post
	@Before({LoginInterceptor.class, TopicValidator.class, PostValidator.class})
    public void save(){
    	Integer userID = getSessionAttr("userID");
        Topic topic = getModel(Topic.class);
        topic.set("userID", userID);
        topic.set("id", null);
        Post post = getModel(Post.class);
        post.set("userID", userID);
        topic.save(post);
        redirect("/post/" + topic.getInt("id"));
    }
	
}

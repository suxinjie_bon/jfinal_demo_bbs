package com.demo.controller;

import com.demo.common.Constants;
import com.jfinal.core.Controller;
import com.jfinal.ext.render.CaptchaRender;

public class CommonController extends Controller {
	
	//获取验证码
    public void code() {
        CaptchaRender img = new CaptchaRender(Constants.RANDOM_CODE_KEY);
        render(img);
    }
}

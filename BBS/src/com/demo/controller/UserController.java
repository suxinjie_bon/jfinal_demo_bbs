package com.demo.controller;

import com.demo.common.Constants;
import com.demo.interceptor.UserCheckInterceptor;
import com.demo.interceptor.UserCheckInterceptorIsImg;
import com.demo.model.User;
import com.demo.validator.LoginValidator;
import com.demo.validator.RegistValidator;
import com.demo.validator.UpdateUserValidator;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;

public class UserController extends Controller {
	
	public void index(){
		Integer userID = getParaToInt(0 ,0);
		User user = User.dao.get(userID);
		setAttr("user", user);
		render("/user/user.html");
	}
	
	//登陆
	@Before(LoginValidator.class)
    public void login(){
        String email = getPara("email");
        String password = getPara("password");
        User user = User.dao.getByEmailAndPassword(email, password);
        if (user != null){
            String bbsID = email + Constants.BBS_ID_SEPARATOR + password;
            setCookie("bbsID", bbsID, 3600*24*30);
            setSessionAttr("user", user);
            setSessionAttr("userID", user.get("id"));
            redirect("/");
        }else{
            setAttr("msg", "用户名或密码错误");
            render("/user/login.html");
        }
    }
    
	//注销
    public void logout(){
        removeSessionAttr("user");
        removeSessionAttr("userID");
        removeCookie("bbsID");
        redirect("/");
    }
	
    //提交注册
	@Before(RegistValidator.class)
    public void save(){
        User user = getModel(User.class);
        user.otherSave();
        setAttr("msg", "注册成功，请登录");
        render("/user/login.html");
    }
	
	//编辑用户信息：只有用户本人才能执行此操作（需要判断请求userID-getParaToInt(0, 0）与session中的userID是否一致）
	@Before(UserCheckInterceptor.class)
	public void edit(){
		setAttr("user", User.dao.get(getParaToInt(0, 0)));
        render("/user/edit.html");
	}
	
	//修改用户信息
	@Before({UserCheckInterceptorIsImg.class ,UpdateUserValidator.class })
	public void update(){
		User user = getModel(User.class);
		UploadFile file = getAttr("imgFile");
		String filePath = "";
		if(null == file)
			filePath = "/img/touxiang1.png";
		else
			filePath = "/upload/" + file.getFileName();
		user.set("headImg", filePath);
		user.myUpdate();
		setAttr("user", user);
		render("/user/user.html");
	}
    
}

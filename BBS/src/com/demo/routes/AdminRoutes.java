/**  
 * 文件名:    AdminRoutes.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:16:09  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */
package com.demo.routes;

import com.demo.controller.AdminController;
import com.demo.controller.ModuleController;
import com.jfinal.config.Routes;

/**  
 * @ClassName: AdminRoutes   
 * @Description: 后端路由集中管理
 * @author: Sue  
 * @date:2015年5月15日 上午10:16:09      
 */
public class AdminRoutes extends Routes {

	@Override
	public void config() {
		 add("/admin/module", ModuleController.class);
		 add("/admin", AdminController.class);
	}
}

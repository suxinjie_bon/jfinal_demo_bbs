/**  
 * 文件名:    FrontRoutes.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:14:01  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */
package com.demo.routes;

import com.demo.controller.CommonController;
import com.demo.controller.IndexController;
import com.demo.controller.PostController;
import com.demo.controller.ReplyController;
import com.demo.controller.TopicController;
import com.demo.controller.UserController;
import com.jfinal.config.Routes;

/**  
 * @ClassName: FrontRoutes   
 * @Description: 前端路由集中管理
 * @author: Sue  
 * @date:2015年5月15日 上午10:14:01      
 */
public class FrontRoutes extends Routes {

	@Override
	public void config() {
		add("/" ,IndexController.class);
		add("/topic" ,TopicController.class);
		add("/user" ,UserController.class);
		add("/common" ,CommonController.class);
		add("/post" ,PostController.class);
		add("/reply" ,ReplyController.class);
	}

}

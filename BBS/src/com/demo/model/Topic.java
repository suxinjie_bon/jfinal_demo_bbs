/**  
 * 文件名:   Topic.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:13:12
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.model;

import java.util.Date;

import com.demo.common.Constants;
import com.demo.extended.jfinal.ExtModel;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

/**  
 * @ClassName: Topic   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月14日 下午10:13:12
 */
public class Topic extends ExtModel<Topic>{
    public static final Topic dao = new Topic();
    private static final String TOPIC_CACHE = "topic";
    private static final String TOPIC_PAGE_FOR_INDEX_CACHE = "topicPageForIndex";
    private static final String TOPIC_PAGE_FOR_MODULE_CACHE = "topicPageForModule";
    private static final String HOT_TOPIC_PAGE_CACHE = "hotTopicPage";
    private static final String NICE_TOPIC_PAGE_CACHE = "niceTopicPage";

    public Topic(){
        super(TOPIC_CACHE);
    }
    
    public Topic get(int id){
        return loadModel(id);
    }
    
    //获取所有帖子-分页
    public Page<Topic> getPage(int pageNumber){
        Page<Topic> topicPage = dao.paginateByCache(TOPIC_PAGE_FOR_INDEX_CACHE, pageNumber,
                pageNumber, Constants.TOPIC_PAGE_SIZE,
                "select id", "from topic order by createTime desc");
        return loadModelPage(topicPage);
    }
    
    //浏览最多（热门）-分页
    public Page<Topic> getHotPage(int pageNumber){
        String cacheName = HOT_TOPIC_PAGE_CACHE;
        Page<Topic> topicPage = dao.paginateByCache(cacheName, pageNumber,
                pageNumber, Constants.TOPIC_PAGE_SIZE,
                "select id", "from topic order by pv desc");
        return loadModelPage(topicPage);
    }
    
    //精华帖子-分页
    public Page<Topic> getNicePage(int pageNumber){
        String cacheName = NICE_TOPIC_PAGE_CACHE;
        Page<Topic> topicPage = dao.paginateByCache(cacheName, pageNumber,
                pageNumber, Constants.TOPIC_PAGE_SIZE,
                "select id", "from topic where isNice=true order by createTime desc");
        return loadModelPage(topicPage);
    }
    
    //内联(表关联)获取关联User，可以在前台Topic.User.XXX
    public User getUser(){
        return User.dao.get(this.getInt("userID"));
    }
    
    //内联(表关联)获取关联Module，可以在前台Topic.Module.XXX
    public Module getModule(){
        return Module.dao.get(this.getInt("moduleID"));
    }
    
    //根据分类获取内容
    public Page<Topic> getPageForModule(int moduleID, int pageNumber){
        Page<Topic> topicPage = dao.paginateByCache(TOPIC_PAGE_FOR_MODULE_CACHE, moduleID + "-" + pageNumber,
                pageNumber, Constants.TOPIC_PAGE_SIZE,
                "select id", "from topic where moduleID=? order by createTime desc", moduleID);
        return loadModelPage(topicPage);
    }
    
    //发表新帖子save-topic和save-post
    public void save(Post post){
    	Date nowDate = new Date();
    	this.set("createTime", nowDate);
    	this.save();
    	
    	post.set("topicID", this.getInt("id"));
    	post.set("createTime", nowDate);
        post.save();
        
        removeAllPageCache();
    }
    
    //浏览量pv + 1
    public void increaseTopicPV(int topicID){
    	Topic topic = dao.findFirst("select pv from topic where id=?", topicID);
    	if(topic != null){
    		int pv = topic.getInt("pv");
    		new Topic().set("id", topicID).set("pv", pv+1).update();
    		increaseTopicAttrInCache(topicID, "pv");//更新缓存
    	}
    }
    
    //回复量+1（楼层数）同时更新cache
    public void increaseTopicPostCount(int topicID){
        int postCount = dao.findFirst("select postCount from topic where id=?", topicID).getInt("postCount");
        new Topic().set("id", topicID).set("postCount", postCount + 1).update();
        increaseTopicAttrInCache(topicID, "postCount");
    }
    private void increaseTopicAttrInCache(int topicID, String attr) {
        CacheKit.put(TOPIC_CACHE, topicID, get(topicID).set(attr, get(topicID).getInt(attr) + 1));
    }
    
    //清除所有缓存
    public void removeAllPageCache() {
        CacheKit.removeAll(TOPIC_PAGE_FOR_INDEX_CACHE);
        CacheKit.removeAll(TOPIC_PAGE_FOR_MODULE_CACHE);
        CacheKit.removeAll(HOT_TOPIC_PAGE_CACHE);
        CacheKit.removeAll(NICE_TOPIC_PAGE_CACHE);
    }
    
}
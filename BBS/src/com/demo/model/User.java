/**  
 * 文件名:    User.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:20:09  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.model;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

import com.demo.extended.jfinal.ExtModel;
import com.jfinal.plugin.ehcache.CacheKit;

/**  
 * @ClassName: User   
 * @Description:   
 * @author: Sue  
 * @date:2015年5月14日 下午10:20:09   
 */
public class User extends ExtModel<User> {
    public static final User dao = new User();
    private static final String USER_CACHE = "user";

    public User() {
        super(USER_CACHE);
    }
    
    public User get(int id) {
        return loadModel(id);
    }
    
    public User getByEmailAndPassword(String email, String password){
        return dao.findFirst("select id, username, email, password from user where email=? and password=?", email, password);
    }
    
    public void otherSave(){
    	this.set("headImg", "");
    	this.set("blogUrl", "");
    	this.set("feeling", "");
    	this.set("registDate", new Date());
    	String password = getMD5(this.getStr("password").getBytes());
    	this.set("password", password);
    	this.save();
    	
    }
    
    public static String getMD5(byte[] src){
        StringBuffer sb=new StringBuffer();
        try {
            java.security.MessageDigest md=java.security.MessageDigest.getInstance("MD5");
            md.update(src);
            for(byte b : md.digest())
                sb.append(Integer.toString(b>>>4&0xF,16)).append(Integer.toString(b&0xF,16));
        } catch (NoSuchAlgorithmException e) {
        	
        }
        return sb.toString();
    }
    
    //邮箱是否已存在
    public boolean containEmail(String email){
    	return dao.findFirst("select email from user where email=? limit 1" ,email) != null;
    }
    
    //用户名是否已存在
    public boolean containUsername(String userName){
    	return dao.findFirst("select username from user where username=? limit 1" ,userName) != null;
    }
    
    //保存用户信息修改
    public void myUpdate() {
        this.set("username", getStr("username"));
        this.set("headImg", getStr("headImg"));
        this.set("blogUrl", getStr("blogUrl"));
        this.set("feeling", getStr("feeling"));
        this.update();
        removeThisCache(this.getInt("id"));
    }
    
    private void removeThisCache(int id){
        CacheKit.remove(USER_CACHE, id);
    }

}

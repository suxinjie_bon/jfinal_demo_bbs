/**  
 * 文件名:    Post.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:05:20  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.model;

import com.demo.common.Constants;
import com.demo.extended.jfinal.ExtModel;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

/**  
 * @ClassName: Post   
 * @Description: 
 * @author: Sue  
 * @date:2015年5月15日 上午10:05:20  
 */
public class Post extends ExtModel<Post> {
    public static final Post dao = new Post();
    private static final String POST_CACHE = "post";
    private static final String POST_PAGE_CACHE = "postPage";


    public Post() {
        super(POST_CACHE);
    }
    
    //分页
    public Page<Post> getPage(int topicID, int pageNumber){
        Topic.dao.increaseTopicPV(topicID);
        String cacheName = POST_PAGE_CACHE;
        Page<Post> postPage = dao.paginateByCache(cacheName, topicID + "-" + pageNumber, pageNumber, Constants.POST_PAGE_SIZE,
                "select id", "from post where topicID=?", topicID);
        return loadModelPage(postPage);
    }
    
    //内联User
    public User getUser(){
        return User.dao.get(this.getInt("userID"));
    }
    
    //内联Reply
    public Page<Reply> getReplyPage() {
        return Reply.dao.getPage(this.getInt("id"), 1);
    }
    
    //内联Topic
    public Topic getTopic(){
        return Topic.dao.get(this.getInt("topicID"));
    }
    
    //保存评论
    public void mySave(){
        this.set("content", this.getStr("content")).save();
        int topicID = this.getInt("topicID");
        Topic.dao.increaseTopicPostCount(topicID);
        removeAllPageCache();
    }
    
    //已经回复
    public void setHasReplyTrue(int postID){
        boolean hasReply = dao.findById(postID).getBoolean("hasReply");
        if ( ! hasReply){
            new Post().set("id", postID).set("hasReply", true).update();
        }
    }

    public void removeAllPageCache() {
        CacheKit.removeAll(POST_PAGE_CACHE);
    }
}

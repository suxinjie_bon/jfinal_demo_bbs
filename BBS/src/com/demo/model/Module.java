/**  
 * 文件名:    Module.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:01:11
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.model;

import java.util.List;

import com.demo.extended.jfinal.ExtModel;
import com.jfinal.plugin.ehcache.CacheKit;

/**  
 * @ClassName: Module   
 * @Description:  
 * @author: Sue  
 * @date:2015年5月15日 上午10:01:11 
 */
public class Module extends ExtModel<Module> {
    public static final Module dao = new Module();
    private static final String MODULE_CACHE = "module";
    private static final String MODULE_LIST_CACHE = "moduleList";

    public Module() {
        super(MODULE_CACHE);
    }
    
    public Module get(int id) {
        return loadModel(id);
    }
    
    public List<Module> getList(){
        return dao.findByCache(MODULE_LIST_CACHE, 1, "select * from module order by turn");
    }

    public void removeAllCache(){
        CacheKit.removeAll(MODULE_CACHE);
        CacheKit.removeAll(MODULE_LIST_CACHE);
    }

}

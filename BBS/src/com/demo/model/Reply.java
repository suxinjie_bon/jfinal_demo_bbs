/**  
 * 文件名:    Reply.java  
 * 描述:   
 * 作者:     Sue
 * 版本:    1.0  
 * 创建时间:   2015年5月15日 上午10:10:01  
 *  
 * 修改历史:  
 * 日期                          作者           版本         描述  
 * ------------------------------------------------------------------  
 *  2015年5月15日      Sue      1.0        1.0 Version  
 */ 
package com.demo.model;

import java.util.Date;

import com.demo.common.Constants;
import com.demo.extended.jfinal.ExtModel;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

/**  
 * @ClassName: Reply   
 * @Description:   
 * @author: Sue  
 * @date:2015年5月14日 下午10:10:01   
 */
public class Reply extends ExtModel<Reply> {
    public static final Reply dao = new Reply();
    private static final String REPLY_CACHE = "reply";
    private static final String REPLY_PAGE_CACHE = "replyPage";


    public Reply() {
        super(REPLY_CACHE);
    }

    public Page<Reply> getPage(int postID, int pageNumber){
        String cacheName = REPLY_PAGE_CACHE;
        Page<Reply> replyPage = Reply.dao.paginateByCache(cacheName, postID + "-" + pageNumber, pageNumber, Constants.REPLY_PAGE_SIZE,
                "select id", "from reply where postID=?", postID);
        return loadModelPage(replyPage);
    }
    
    public void mySave(int postID){
        Post.dao.setHasReplyTrue(postID);
        this.set("createTime", new Date()).set("content" ,this.getStr("content")).save();
        removeAllPageCache();
    }
    
    public Page<Reply> getLastPage(int postID){
        int totalPage = getPage(postID, 1).getTotalPage();
        return loadModelPage(getPage(postID, totalPage));
    }
    
    //内联
    public User getUser(){
        return User.dao.get(this.getInt("userID"));
    }
    //内联
    public Topic getTopic(){
        return Topic.dao.get(this.getInt("topicID"));
    }
    
    public void removeAllPageCache() {
        CacheKit.removeAll(REPLY_PAGE_CACHE);
    }
    
}
